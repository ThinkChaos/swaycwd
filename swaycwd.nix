{ pkgs, nimPackages, stdenv, lib,
enableShells ? [ "bash" "zsh" "fish" "sh" "posh" ] 
}:
nimPackages.buildNimPackage {
    name = "swaycwd";
    src = ./.;

    patchPhase = ''
        {
        echo 'let enabledShells: seq[string] = @${builtins.toJSON enableShells}'
        echo 'export enabledShells'
        } | tee src/shells.nim
    '';

    meta = with lib; {
        description = ''
        Returns cwd for shell in currently focused sway window,
        or home directory if cannot find shell.
        '';
        license = lib.licenses.gpl3;
    };

}
