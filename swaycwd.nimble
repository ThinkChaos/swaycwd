# Package

version       = "0.2.1"
author        = "cab"
description   = "xcwd but wayland"
license       = "GPL-3.0-or-later"
srcDir        = "src"
bin           = @["swaycwd"]


# Dependencies

requires "nim >= 1.6.6"
