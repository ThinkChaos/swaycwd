{
  outputs = { self, nixpkgs }:
    let
      onPkgs = f: builtins.mapAttrs f nixpkgs.legacyPackages;
    in
    {
      packages = onPkgs (system: pkgs: { default = pkgs.callPackage ./. { }; });
      devShells = onPkgs (system: pkgs: with pkgs; {
        default = mkShell {
          buildInputs = [
            nixpkgs-fmt
          ];
          inputsFrom = [
            self.packages.${system}.default
          ];
        };
      });
    };
}
